package classes;

import java.util.ArrayList;
import java.util.Arrays;


public class PointList {

    private ArrayList<Point> pointList = new ArrayList<>();

    public void add(Point point) {
        this.pointList.add(point);
    }

    public Point[] toArray() {
        return this.pointList.toArray(new Point[0]);
    }

    @Override
    public String toString() {

        StringBuilder result = new StringBuilder();

        for (Point p : this.pointList) {
            result.append(p.toString()).append("\n");
        }

        return result.toString();

    }

    @Override
    public boolean equals(Object obj) {

        PointList pointList;

        if (!(obj instanceof PointList))
            return false;
        else
            pointList = (PointList) obj;

        return Arrays.equals(this.toArray(), pointList.toArray());

    }

}
