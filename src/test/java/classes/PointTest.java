package classes;

import org.junit.Test;

import static org.junit.Assert.*;


public class PointTest {

    @Test
    public void getDistance() {

        double delta = 0.0001;

        assertEquals(0.0, new Point(0, 0).getDistance(new Point(0, 0)), delta);
        assertEquals(0.0, new Point(+0, 0).getDistance(new Point(0, -0)), delta);
        assertEquals(0.0, new Point(-0.0, -0.0).getDistance(new Point(0.0, 0.0)), delta);
        assertEquals(5.0, new Point(0, 0).getDistance(new Point(3, 4)), delta);
        assertEquals(13.0, new Point(0, 0).getDistance(new Point(5, 12)), delta);
        assertEquals(19.177, new Point(-15.5, 2.1).getDistance(new Point(3.5, -0.5)), delta);
        assertEquals(103.928, new Point(-110.5, -85).getDistance(new Point(-50.7, 0)), delta);
        assertEquals(
                Double.POSITIVE_INFINITY,
                new Point(Double.MIN_VALUE, Double.MIN_VALUE).getDistance(
                        new Point(Double.MAX_VALUE, Double.MIN_VALUE)
                ), delta
        );

    }

    @Test
    public void testToString() {

        assertEquals("Point: (0.0, 0.0)", new Point(0, 0).toString());
        assertEquals("Point: (0.0, -0.0)", new Point(-0, -0.0).toString());
        assertEquals("Point: (0.0, 0.0)", new Point(+0, +0.0).toString());
        assertEquals("Point: (0.0, 0.0)", new Point(0.0, 0.0).toString());
        assertEquals("Point: (1.005, 0.1)", new Point(1.005, 0.1).toString());
        assertEquals("Point: (1.7976931348623157E308, 4.9E-324)", new Point(Double.MAX_VALUE, Double.MIN_VALUE).toString());
        assertEquals("Point: (3.0156, -5.0)", new Point(3.0156, -5).toString());
        assertEquals("Point: (0.001, -0.01)", new Point(.0010, -.01).toString());
        assertEquals("Point: (0.05, -4.01)", new Point(.05d, -4.01d).toString());

    }

    @Test
    public void testEquals() {

       assertEquals(new Point(-5.4, 1.2), new Point(-5.4, 1.2));
       assertEquals(new Point(0.0, 0), new Point(-0.0, +0.0));
       assertEquals(new Point(Double.MIN_VALUE, Double.MAX_VALUE), new Point(Double.MIN_VALUE, Double.MAX_VALUE));

       assertNotEquals(new Point(7.5, 1), new Point(-12, 0.1));
       assertNotEquals(new Point(2.5, 3), new Point(3, 2.5));
       assertNotEquals(new Point(0.01, 1.005), new Point(0.001, 1.00501));

       assertNotEquals(new Point(2.4, 3.35), new StringBuilder("string"));

    }

}
