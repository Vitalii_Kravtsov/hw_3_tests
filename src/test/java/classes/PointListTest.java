package classes;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;


public class PointListTest {

    @Test
    public void add() {

        PointList pointList = new PointList();
        pointList.add(new Point(2.5, -50.2));

        assertArrayEquals(new Point[] {new Point(2.5, -50.2)}, pointList.toArray());

        pointList.add(new Point(0.0, -0.0));

        assertArrayEquals(new Point[] {new Point(2.5, -50.2), new Point(0.0, 0.0)}, pointList.toArray());

    }

    @Test
    public void toArray() {

        PointList pointList = new PointList();

        assertArrayEquals(pointList.toArray(), new Point[] {});

        pointList.add(new Point(-3.75, -2.25));
        pointList.add(new Point(15.125, 81));

        assertArrayEquals(new Point[] {new Point(-3.75, -2.25), new Point(15.125, 81)}, pointList.toArray());

    }

    @Test
    public void testToString() {

        PointList pointList = new PointList();

        assertEquals("", pointList.toString());

        pointList.add(new Point(7.7, -2.2));
        pointList.add(new Point(0, -0.0));

        String expected = "Point: (7.7, -2.2)\nPoint: (0.0, -0.0)\n";

        assertEquals(expected, pointList.toString());

    }

    @Test
    public void testEquals() {

        PointList list1 = new PointList();
        PointList list2 = new PointList();

        assertEquals(list1, list2);

        list1 = new PointList();
        list1.add(new Point(-0.5, -2.2));
        list1.add(new Point(15, 9));

        list2 = new PointList();
        list2.add(new Point(-0.5, -2.2));
        list2.add(new Point(15, 9));

        assertEquals(list1, list2);

        list1 = new PointList();

        list2 = new PointList();
        list2.add(new Point(-0.01, 3.5));

        assertNotEquals(list1, list2);

        assertNotEquals(list1, new ArrayList<Point>());

    }

    @Test
    public void testEqualsWithOrder() {

        PointList list1 = new PointList();
        list1.add(new Point(1.2, -5.06));
        list1.add(new Point(13.2, -100));
        list1.add(new Point(-13.4, 102));

        PointList list2 = new PointList();
        list2.add(new Point(13.2, -100));
        list2.add(new Point(-13.4, 102));
        list2.add(new Point(1.20, -5.06));

        assertEquals(list1, list2);

    }

}
