package classes;

import org.junit.Test;

import static org.junit.Assert.*;


public class CircleTest {

    @Test
    public void testConstructor() {
        assertThrows(ArithmeticException.class, () -> {new Circle(new Point(-3.2, 5.01), -5);});
    }

    @Test
    public void testInbounding() {

        PointList all = new PointList();
        all.add(new Point(0.1, 0.5));
        all.add(new Point(2, 2.4));
        all.add(new Point(-1.02, -0.0));
        all.add(new Point(1.999, 0.0));
        all.add(new Point(2, 0));
        all.add(new Point(-0.0, 2.0));
        all.add(new Point(2 * Math.cos(Math.PI / 4), 2 * Math.cos(Math.PI / 4)));
        all.add(new Point(100.5, 25.6));

        PointList inbouding = new PointList();
        inbouding.add(new Point(0.1, 0.5));
        inbouding.add(new Point(-1.02, -0.0));
        inbouding.add(new Point(1.999, 0.0));

        Circle circle = new Circle(new Point(0.0, 0.0), 2.0);

        assertEquals(inbouding, circle.inbounding(all));

        all = new PointList();
        all.add(new Point(0, 0.0));
        all.add(new Point(-12.5, 1.85));
        all.add(new Point(-13, -3.5));
        all.add(new Point(Double.MAX_VALUE, Double.MIN_VALUE));

        inbouding = new PointList();
        inbouding.add(new Point(0, 0.0));
        inbouding.add(new Point(-12.5, 1.85));

        circle = new Circle(new Point(-5.2, 3.5), 7.5);

        assertEquals(inbouding, circle.inbounding(all));

    }

    @Test
    public void testInboundingZero() {

        PointList all = new PointList();
        all.add(new Point(0.3, -0.01));
        all.add(new Point(0.01, -0.01));
        all.add(new Point(-15, -0.0));
        all.add(new Point(0.0, -0.0));
        all.add(new Point(100.5, -200));

        PointList inbouding = new PointList();
        inbouding.add(new Point(0.0, 0.0));

        Circle circle = new Circle(new Point(0.0, -0.0), 0.0);

        assertEquals(inbouding, circle.inbounding(all));

    }

    @Test
    public void testEquals() {

        assertEquals(new Circle(new Point(2.2, 5.5), 13.01), new Circle(new Point(2.2, 5.5), 13.01));
        assertEquals(new Circle(new Point(1.01, 3.02), 0.0), new Circle(new Point(1.01, 3.02), -0.0));
        assertEquals(new Circle(new Point(0.0, -.0), +0.0), new Circle(new Point(0, 0), -0.0));
        assertEquals(new Circle(new Point(13.5, -4), +0.0), new Circle(new Point(13.5, -4.0), -0.0));
        assertEquals(new Circle(new Point(2.4, -2), Double.MAX_VALUE), new Circle(new Point(2.4, -2), Double.MAX_VALUE));

        assertNotEquals(new Circle(new Point(7.04, -10), 2.2), new Circle(new Point(17.5, -100.0), 0.01));
        assertNotEquals(new Circle(new Point(3.3, -2.2), 2.2), new Circle(new Point(2.2, 3.3), 2.2));
        assertNotEquals(new Circle(new Point(0.001, 3.003), 5.001), new Circle(new Point(0.001, 3.003), 5.001001));

        assertNotEquals(new Circle(new Point(1.1, -2.5), 3.05), new Point(1.1, -2.5));

    }

}
